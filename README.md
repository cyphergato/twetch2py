# Twetch2py
A class-based library for using the [Twetch SDK](https://www.npmjs.com/package/@twetch/sdk) with Python.

## Installation
1. Install the Twetch SDK in CLI mode:
```
npm install -g @twetch/sdk
```
2. Initialize the Twetch SDK from the command line:
```
twetch init
```
3. Install `twetch2py` on pip:
```
pip3 install twetch2py
```
4. Import `twetch2py` into your project:
```
from twetch2py import Twetch
```
5. Authenticate your signing address on the [Twetch developer page](https://twetch.app/developer). Don't forget to fund your wallet!

## Usage
Once instantiated, Twetch objects have the following attributes:
1. `content`: A string for text-based posts. Accepts @ and branch URLs.
2. `media`: Filename for rich media (like images). Relative paths will be converted to absolute before publishing.
3. `reply_tx`: A transaction ID of a post to reply to.
4. `like_tx`: A transaction ID of a post to like.
5. `published_url`: Successful posts and likes will set the URL of your Twetch to this attribute.
6. `tweet`: Boolean. If `True`, the target Twetch will be cross-posted to Twitter.
7. `hide_link`: Boolean. If true and `tweet` also evaluets to `True`, the Twetch link will be hidden on Twitter.

The library can also fetch the following information about your Twetch SDK install:
1. `get_balance()`: Returns your signing address balance in BSV.
2. `get_address()`: Returns the signing address of your Twetch SDK BSV wallet.

## Examples
```
# Make a text post
my_twetch = Twetch(content="Hello from Python!")
my_twetch.publish()

# Branch a post
my_branch = Twetch(content="https://twetch.app/t/b9ca41ea6e7302016ab039babe19548f74feccdbd2addbc1c50b3e49907e6ef4")
my_branch.publish()

# Quote a post
my_quote = Twetch(content="This is a quote branch! https://twetch.app/t/b9ca41ea6e7302016ab039babe19548f74feccdbd2addbc1c50b3e49907e6ef4")
my_quote.publish()

# Post an image
my_image_twetch = Twetch(content="Image from Python!", media="images/myimage.jpg")
my_image_twetch.publish()

# Reply to a post
my_reply = Twetch(content="Hello fellow Twetcher!", reply_tx="b9ca41ea6e7302016ab039babe19548f74feccdbd2addbc1c50b3e49907e6ef4")
my_reply.publish()

# Reply to a post w/image and text
my_reply = Twetch(content="Hello with image!", media="images/myimage.jpg", reply_tx="b9ca41ea6e7302016ab039babe19548f74feccdbd2addbc1c50b3e49907e6ef4")
my_reply.publish()

# Tweet from Twetch
my_twetch = Twetch(content="Hello Twitter from Twetch!", tweet=True)
my_twetch.publish()

# Tweet from Twetch, hide Twetch link on Twitter
my_twetch = Twetch(content="Hello Twitter from Twetch!", tweet=True, hide_link=True)
my_twetch.publish()

# Like a post
my_like = Twetch(like_tx="b9ca41ea6e7302016ab039babe19548f74feccdbd2addbc1c50b3e49907e6ef4")
my_like.like_twetch()

# Print your active signing address (returns a string)
twetch_object = Twetch()
print(twetch_object.get_address())

# Print the balance of your signing wallet (returns a float)
twetch_object = Twetch()
print(twetch_object.get_balance())
```
## License
Twetch2py is offered by Cyphergato, LLC. under the [MIT License](https://en.wikipedia.org/wiki/MIT_License) and is free to use.

If you find this open source library useful, please consider donating to support its development!
- Paymail: `cyphergato@moneybutton.com`
- Address: `12Jm4J1GxWfA52JdiYHLovhwaFmy89LD6Q`